### Remote PI ###

![goM8BL.png](https://bitbucket.org/repo/5La45o/images/280909689-goM8BL.png)

GPIO pinlerini web uygulaması olarak yönetmek için geliştirilmiş küçük bir araç.

### Kullanımı ###
**Web** klasörü içerisindeki dosyaları sunucunuza yükledikten sonra istemcinizde **remotepi.py** dosyasını çalıştırın ve sunucunuza yüklediğiniz uygulama üzerinden pinleri aktif ederek çalışabilirliğini kontrol edin.